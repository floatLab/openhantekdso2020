#pragma once

#include "dsomodel.h"

class HantekDsoControl;
using namespace Hantek;

struct ModelDSO2020 : public DSOModel {
    static const int ID = 0x6022;
    ModelDSO2020();
    virtual void applyRequirements(HantekDsoControl* dsoControl) const override;
};
